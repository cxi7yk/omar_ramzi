
#include <stdio.h>
#include <stdlib.h>

int tele_val(char *numb);
void check (int results);

int tele_val(char *numb){
    if (!numb || (numb =='\0'))
        return 0;

    int count = 0;
    int only_num = 0;
    int c;

    while((c=*numb++)){
        count++;
        if (c < '0' || c > '9'){
            only_num = 1;
            break;
        }

    }

    if(count == 10){
        if(only_num != 1){
            check(0);
        }
        else{
            check(2);
        }
    }
    else{
        check(1);
    }

    return 0;

}

void check (int results){
    switch (results){
    case 1:
        printf("Minimum length is 10\n");
        break;
    case 2:
        printf("Must contain only numbers");
        break;
    default:
        printf("Phone number is valid");
    }
}
